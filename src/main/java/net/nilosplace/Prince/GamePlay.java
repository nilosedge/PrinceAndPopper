package net.nilosplace.Prince;

import java.util.*;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;
import net.nilosplace.Prince.Players.Player;

public class GamePlay extends Thread {
	
	private Dealer deal;
	private List<Player> players;
	private Move cm;
	private int winpos = 0;
	private int moveCount = 0;

	public GamePlay(Dealer deal, List<Player> players) {
		this.deal = deal;
		this.players = players;
	}
	
	public void run() {
		while(true) {
			while(true) {
				for(int i = 0; i < players.size(); i++) {
					Player p = players.get(i);
					
					if(!p.isWon()) {
						
						if((cm != null && cm.playerId == p.getId()) || cm == null) {
							p.youWonRound();
							cm = null;
						}
	
						Move m = p.getPlayerMove(cm);
						moveCount++;
						if(m != null && m.type == MoveType.CARD) {
							m.playerId = p.getId();
							if(deal.checkMove(p.getId(), cm, m)) {
								m.winningMove = deal.makeMove(p.getId(), m);
								if(m.winningMove) {
									m.winningPos = winpos;
									p.setIsWon(true);
									p.setNewid(winpos);
									p.printStats();
									winpos++;
								}
								p.moveAccepted();
								cm = m;
							} else {
								System.out.println("Move Rejected: " + m);
								p.printCards();
								m.type = MoveType.PASS;
								m.winningMove = false;
								p.moveRejected();
							}
							sendMoves(m);
						} else {
							m = new Move();
							m.type = MoveType.PASS;
							m.playerId = p.getId();
							sendMoves(m);
						}
					} else if(cm != null && p.isWon() && cm.playerId == p.getId()) {
						cm = null;
					}
				}
				if(deal.gameIsOver()) {
					System.out.println("Game is over, number of moves: " + moveCount);
					break;
				}
			}
			winpos = 0;
			moveCount = 0;
			cm = null;
			reOrderPlayers();
			deal.newGame();
			//try {
			//	sleep(0);
			//} catch (InterruptedException e) {
			//	// TODO Auto-generated catch block
			//	e.printStackTrace();
			//}
		}
	}
	
	public void sendMoves(Move m) {
		for(Player p: players) {
			if(!p.isWon() && p.getId() != m.playerId) {
				p.movePlayer(m);
			}
		}
	}

	public void reOrderPlayers() {
		List<Player> newList = new ArrayList<Player>();
		for(int i = 0; i < players.size(); i++) {
			for(Player p: players) {
				if(p.getNewid() == i) {
					p.setId(i);
					newList.add(p);
					break;
				}
			}
		}
		players = newList;
		deal.setPlayers(players);
	}

}
