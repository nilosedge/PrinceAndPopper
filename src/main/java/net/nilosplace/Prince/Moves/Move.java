package net.nilosplace.Prince.Moves;

public class Move {
	
	public int playerId;
	public int cardNumber;
	public int amount;
	public MoveType type;
	public boolean winningMove = false;
	public int winningPos = 0;
	public boolean playMovesToNextPlayer = false;
	
	public Move() {}
	
	public Move(int playerId, int cardNumber, int amount, MoveType type) {
		this.playerId = playerId;
		this.cardNumber = cardNumber;
		this.amount = amount;
		this.type = type;
	}
	
	public Move(Move m) {
		if(m != null) {
			this.playerId = m.playerId;
			this.cardNumber = m.cardNumber;
			this.amount = m.amount;
			this.type = m.type;
			this.winningMove = m.winningMove;
			this.winningPos = m.winningPos;
			this.playMovesToNextPlayer = m.playMovesToNextPlayer;
		}
	}
	
	public String toString() {
		String ret = "";
		ret += "p: " + playerId + " ";
		ret += "c: " + cardNumber + " ";
		ret += "a: " + amount + " ";
		ret += "t: " + type + " ";
		//ret += "winningMove: " + winningMove + " ";
		//ret += "winningPos: " + winningPos + " ";
		//ret += "playMovesToNextPlayer: " + playMovesToNextPlayer;
		return ret;
	}

	public enum MoveType {
		PASS, CARD
	}
}
