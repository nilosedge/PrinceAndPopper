package net.nilosplace.Prince.Players;

import java.util.*;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;

public abstract class Player {
	
	protected int id = 0;
	private boolean isWon = false;
	private int newid = 0;
	protected int[] hand;
	protected int numbersOnCard = 0;
	private int numbersOfDecks = 0;
	private int playerCount = 0;
	
	private double gamesPlayed = 0;
	private double gamesPos = 0;
	private double gamesAvg = 0;
	
	private Move lastMove;
	private Move nextMove;

	public Player(int id) { this.id = id; }
	
	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	public boolean isWon() { return isWon; }
	public void setIsWon(boolean isWon) { this.isWon = isWon; }
	public int getNewid() { return newid; }
	public void setNewid(int newid) {
		gamesPlayed++;
		gamesPos += newid;
		gamesAvg = gamesPos / gamesPlayed;
		this.newid = newid;
	}
	
	public void printStats() {
		System.out.println("Player: " + getClass().getSimpleName() + " Games Player: " + gamesPlayed + " Game Pos: " + gamesPos + " Avg Pos: " + gamesAvg);
	} 
	
	public abstract void playerMoved(Move move);
	public abstract Move getYourMove(Move currentMove);
	
	public void moveRejected() {}
	
	public void movePlayer(Move move) {
		if(move.type == MoveType.CARD) {
			lastMove = move;
		}
		playerMoved(move);
	}
	
	public void setCards(int[] hand, int numbersOnCard, int numbersOfDecks, int playerCount) {
		this.hand = hand;
		this.numbersOnCard = numbersOnCard;
		this.numbersOfDecks = numbersOfDecks;
		this.playerCount = playerCount;
		this.isWon = false;
		this.newid = 0;
		init();
	}

	public void youWonRound() {
		lastMove = null;
	}

	public void init() {
		lastMove = null;
		nextMove = null;
	}
	
	public void printCards() {
		for(int i = 0; i < hand.length; i++) {
			if(hand[i] > 0) {
				System.out.print("Card(" + i + ")=" + hand[i] + " ");
			}
		}
		System.out.println();
	}
	
	public void moveAccepted() {
		hand[nextMove.cardNumber] -= nextMove.amount;
	}

	protected List<Move> getMoves() {
		List<Move> moves = new ArrayList<Move>();
		if(lastMove == null) {
			for(int i = 0; i < numbersOnCard; i++) {
				for(int j = 1; j <= hand[i]; j++) {
					if(j == hand[i]) {
						Move m = new Move(id, i, j, MoveType.CARD);
						moves.add(m);
					}
				}
			}
		} else {
			for(int i = 0; i < numbersOnCard; i++) {
				if(
					i > lastMove.cardNumber &&
					hand[i] >= lastMove.amount
				) {
					Move m = new Move(id, i, lastMove.amount, MoveType.CARD);
					moves.add(m);
				}
			}
		}
		return moves;
	}

	public Move getPlayerMove(Move cm) {
		nextMove = getYourMove(new Move(cm));
		return nextMove;
	}
}
