package net.nilosplace.Prince.Players;

import java.util.List;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;


public class SpendyPlayer extends Player {

	public SpendyPlayer(int id) {
		super(id);
	}


	public void playerMoved(Move move) {

	}


	public Move getYourMove(Move currentMove) {
		List<Move> moves = getMoves();
		if(moves.size() == 0) {
			return new Move(id, 0, 0, MoveType.PASS);
		}
		return moves.get(0);
	}


}
