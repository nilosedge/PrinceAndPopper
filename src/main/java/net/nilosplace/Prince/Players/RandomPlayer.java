package net.nilosplace.Prince.Players;

import java.util.List;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;


public class RandomPlayer extends Player {
	
	public RandomPlayer(int id) {
		super(id);
	}

	public void playerMoved(Move move) {

	}

	public Move getYourMove(Move currentMove) {
		List<Move> moves = getMoves();
		if(moves.size() == 0) {
			Move m = new Move(id, 0, 0, MoveType.PASS);
			moves.add(m);
		}
		int move = (int)(Math.random() * moves.size());
		return moves.get(move);
	}


}
