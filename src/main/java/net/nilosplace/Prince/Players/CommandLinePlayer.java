package net.nilosplace.Prince.Players;

import java.io.*;
import java.util.List;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;

public class CommandLinePlayer extends Player {
	
	Move lastMove;

	public CommandLinePlayer(int id) {
		super(id);
	}

	public Move getYourMove(Move currentMove) {
		List<Move> moves = getMoves();
		if(moves.size() == 0) return new Move(id, 0, 0, MoveType.PASS);
		
		String command = readLine();
		Move m = new Move(id, 0, 0, MoveType.PASS);
		System.out.println();
		
		while(!command.startsWith("move ")) {
		
			if(command.equals("pass")) {
				return m;
			} else if(command.equals("moves")) {
				int c = 0;
				for(Move m1: moves) {
					System.out.println("Move " + c + ": " + m1);
					c++;
				}
			} else if(command.equals("quit")) {
				System.exit(0);
			} else if(command.equals("cards")) {
				printCards();
			}
			command = readLine();
		}
		if(command.startsWith("move ")) {
			 String[] array = command.split(" ");
			 m = moves.get(Integer.parseInt(array[1]));
			 System.out.println("Making Move: " + m);
		}
		return m;
	}
	
	private String readLine() {
		String line = "";
		try {
//			int ch;
//			do {
//				ch=in.read();
//				if (ch<0) return;
//				System.out.print((char)ch);
//				System.out.flush();
//			} while(true);
			BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
			if((line = read.readLine()) != null) {
				System.out.println(line);
				System.out.flush();
			}
		} catch (Exception e) {
			   e.printStackTrace();
		}
		return line;
	}

	public void playerMoved(Move move) {
		if(move.type != MoveType.PASS) {
			lastMove = move;
			System.out.println("Move: " + move);
		}
	}

}
