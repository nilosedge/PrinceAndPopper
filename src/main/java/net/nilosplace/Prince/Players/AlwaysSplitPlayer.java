package net.nilosplace.Prince.Players;

import java.util.List;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;


public class AlwaysSplitPlayer extends Player {

	public AlwaysSplitPlayer(int id) {
		super(id);
	}

	public void playerMoved(Move move) {

	}

	public Move getYourMove(Move currentMove) {
		List<Move> moves = getMoves();
		List<Move> filter = getMoves();
		int top = numbersOnCard;
		
		for(Move m: moves) {
			if(currentMove != null && hand[m.cardNumber] >= currentMove.amount) {
				if(m.cardNumber < top) top = m.cardNumber;
				filter.add(m);
			} else if(hand[m.cardNumber] >= m.amount) {
				if(m.cardNumber < top) top = m.cardNumber;
				filter.add(m);
			}
		}
		
		Move nextMove = new Move(id, 0, 0, MoveType.PASS);
		if(filter.size() > 0 && top < numbersOnCard) {
			for(Move m: filter) {
				if(m.cardNumber == top) {
					return m;
				}
			}
		}
		return nextMove;
	}
	
}
