package net.nilosplace.Prince;

import java.util.*;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;
import net.nilosplace.Prince.Players.Player;

public class Dealer {
	
	private int numbersOnCard = 12;
	private int amountOfEachCard = 12;
	private int numbersOfDecks = 0;
	private int totalCards = 0;
	private int thisGameCardCount = 0;
	private int[] deck = new int[numbersOnCard];
	private int[][] hands;
	private int[][] oldhands;
	
	private List<Player> players;

	public Dealer(List<Player> players) {
		this.players = players;
		numbersOfDecks = ((int)(players.size() / 8)) + 1;
		totalCards = numbersOnCard * amountOfEachCard * numbersOfDecks;
		thisGameCardCount = totalCards;
		//totalCards = players.size() * numbersOnCard;
		hands = new int[players.size()][numbersOnCard];
		oldhands = new int[players.size()][numbersOnCard];
		createDeck();
		shuffle();
		saveOldHand();
		distTaxes();
		sendHands();
	}
	
	public void newGame() {
		thisGameCardCount = totalCards;
		createDeck();
		//shuffle();
		useOldHand();
		distTaxes();
		sendHands();
	}

	public void shuffle() {
		for(int i = 0; i < numbersOnCard; i++) {
			deck[i] = amountOfEachCard * numbersOfDecks;
		}
		while(thisGameCardCount > 0) {
			for(int i = players.size() - 1; i >= 0 && thisGameCardCount > 0; i--) {
				hands[i][getCard()]++;
			}
		}
	}

	public void distTaxes() {
		int numberOfPlayersToPayTaxes = 1 + numbersOfDecks;
		int noptpt = numberOfPlayersToPayTaxes;
		// The Number of players having to switch cards
		for(int j = noptpt; j > 0; j--) {
			int popperplayer = noptpt - j + 1;
			int kingplayer = noptpt - j;
			// The Number of times to switch cards
			for(int k = j; k > 0; k--) {
				// Pay to the rich bestcard
				int bestcard = numbersOnCard - 1;
				for(int i = bestcard; i > 0; i--) {
					if(hands[players.size() - popperplayer][i] != 0) {
					hands[players.size() - popperplayer][i]--;
					hands[kingplayer][i]++;
					break;
					}
				}
				// Pay to the poor worst card
				int worstcard = 0;
				for(int i = worstcard; i < numbersOnCard; i++) {
					if(hands[kingplayer][i] != 0) {
						hands[kingplayer][i]--;
						hands[players.size() - popperplayer][i]++;
						break;
					}
				}
			}
		}
	}

	public void sendHands() {
		for(int i = 0; i < players.size(); i++) {
			int[] hand = new int[numbersOnCard];
			int[] work = hands[i];
			for(int j = 0; j < numbersOnCard; j++) {
				hand[j] = work[j];
			}
			players.get(i).setCards(hand, numbersOnCard, numbersOfDecks, players.size());
		}
	}
	
	private void createDeck() {
		for(int i = 0; i < players.size(); i++) {
			for(int j = 0; j < numbersOnCard; j++) {
				hands[i][j] = 0;
			}
		}
	}
	
	private void useOldHand() {
		for(int i = 0; i < players.size(); i++) {
			for(int j = 0; j < numbersOnCard; j++) {
				hands[i][j] = oldhands[i][j];
			}
		}
	}
	
	private void saveOldHand() {
		for(int i = 0; i < players.size(); i++) {
			for(int j = 0; j < numbersOnCard; j++) {
				oldhands[i][j] = hands[i][j];
			}
		}
	}
	
	private int getCard() {
		int ret = (int)(Math.random() * numbersOnCard);
		if(deck[ret] == 0) {
			for(int i = ret; i < (numbersOnCard * 2); i++) {
				ret = (i % numbersOnCard);
				if(deck[ret] != 0) break;
			}
		}
		deck[ret]--;
		thisGameCardCount--;
		return ret;
	}

	public void showHands() {
		System.out.println("Showing All Players Hands: ");
		for(int i = 0; i < players.size(); i++) {
			System.out.print("Player: " + i + " ");
			for(int j = 0; j < numbersOnCard; j++) {
				System.out.print("Card(" + j + ")=" + hands[i][j] + " ");
			}
			System.out.println();
		}
	}
	
	public List<Move> getStartMoves(int playerid) {
		List<Move> moves = new ArrayList<Move>();
		for(int i = 0; i < numbersOnCard; i++) {
			for(int j = 1; j <= hands[playerid][i]; j++) {
				Move m = new Move();
				m.playerId = playerid;
				m.amount = j;
				m.cardNumber = i;
				moves.add(m);
			}
		}
		return moves;
	}
	
	/*
	public List<Move> getMoves2(int playerid, Move winningMove) {
		List<Move> moves = new ArrayList<Move>();
		System.out.println(winningMove);
		for(int i = 0; i < numbersOnCard; i++) {
			if(
				i > winningMove.cardNumber &&
				hands[playerid][i] >= winningMove.amount
			) {
				Move m = new Move();
				m.playerId = playerid;
				m.amount = winningMove.amount;
				m.cardNumber = i;
				moves.add(m);
			}
		}
		return moves;
	}
	
	*/
	public boolean makeMove(int playerid, Move m) {
		if(m.type == MoveType.CARD) {
			hands[playerid][m.cardNumber] -= m.amount;
		}
		return playerHasWon(playerid);
	}
	/*
	public void unMakeMove(int playerid, Move m) {
		if(m.type == MoveType.CARD) {
			hands[playerid][m.cardNumber] += m.amount;
		}
	}
	*/
	
	public boolean playerHasWon(int playerid) {
		int count = 0;
		for(int i = 0; i < numbersOnCard; i++) {
			count += hands[playerid][i];
		}
		return count == 0;
	}
	/*
	private void showCards(int playerid) {
		System.out.println("Player: " + playerid);
		for(int i = 0; i < hands[playerid].length; i++) {
			System.out.print("Card(" + i + ")=" + hands[playerid][i] + " ");
		}
		System.out.println();
	}
	*/
	public boolean checkMove(int playerid, Move winningMove, Move m) {
		if(m.type == MoveType.PASS) return true;
		if(winningMove == null) {
			if(hands[playerid][m.cardNumber] >= m.amount) {
				return true;
			} else {
				return false;
			}
		}
		
		if(
			m.cardNumber > winningMove.cardNumber &&
			m.amount == winningMove.amount && 
			hands[playerid][m.cardNumber] >= m.amount
		) {
			return true;
		}
		//System.out.println("End of the road should never get this error!!!!!!!!!!!!!");
		//System.out.println("Winning: " + winningMove);
		//System.out.println("Move: " + m);
		//showCards(playerid);
		return false;
	}

	public boolean gameIsOver() {
		int count = 0;
		for(int k = 0; k < players.size(); k++) {
			for(int i = 0; i < numbersOnCard; i++) {
				count += hands[k][i];
			}
		}
		return count == 0;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}

}
