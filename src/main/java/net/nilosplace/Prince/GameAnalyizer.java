package net.nilosplace.Prince;

import java.io.*;
import java.util.*;

import net.nilosplace.Prince.Moves.Move;
import net.nilosplace.Prince.Moves.Move.MoveType;

public class GameAnalyizer {
	
	private int numbersOnCard = 12;
	private int amountOfEachCard = 12;
	private int totalCards = numbersOnCard * amountOfEachCard;
	private int thisGameCardCount = 0;
	private int[][] hands;
	private int players = 4;
	private int startDepth = 10;
	private int depth = startDepth;
	private int roundWinner = -1;
	private int roundWinAmount = -1;
	private int roundWinCardNo = -1;
	private int playingAs = 0;
	private LinkedList<Move> q = new LinkedList<Move>();
	private PrintWriter out;
	private int alpha = -100000000;
	private int beta = 100000000;
	
	public static void main(String[] args) {
		new GameAnalyizer();
	}
	
	public GameAnalyizer() {
		try {
			out = new PrintWriter(new File("/Users/olinblodgett/Desktop/pp.log"));
			initCardsStatic();
			//initCards();
			//payTaxes();
				//showCards(0);
				//out.flush();
				//System.exit(0);
			int startPlayer = 0;
			int eval = 0;
			eval = playMove(startPlayer, depth, 0, (startPlayer == playingAs), alpha, beta);
			System.out.println("Eval: " + eval);
			out.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	private int playMove(int playerid, int terminalDepth, int methodDepth, boolean maxPlayer, int alpha, int beta) throws InterruptedException {
		
		if(terminalDepth == 0) {
		
			int ret = 0;
			for(int i = 0; i < numbersOnCard; i++) {
				//System.out.println("Ret: " + amountOfEachCard);
				ret += ((amountOfEachCard - i) * (numbersOnCard - hands[playingAs][i]));
				//System.out.println("Hand: " + hands[playerid][i] + " Ret: " + ret);
			}
			//showCards(methodDepth);
			//System.out.println("Player: " + playerid + " Ret: " + ret);
			//Thread.sleep(1250);
			//System.out.println("We are here now what?");
			return ret;
		}

		int nextPlayerIndex = ((playerid + 1) % players);
		List<Move> moves = getMoves(playerid);
		int ret = 0;
		int aj = 0;
		
		for(Move m: moves) {
			playMove(m);
			
			int nextDepth = 1;
			if(m.type == MoveType.PASS) {
				nextDepth = 0;
			}
			
			//if(methodDepth == 91) {
				//print("P: " + m.playerId + " M: {" + m + "} A:" + alpha + " B:" + beta, methodDepth);
			//}
			
			if(playerHasWon(playerid)) {
				if(playerid == playingAs) {
					ret = 1000000;
				} else if(playerid != playingAs) {
					ret = -1000000;
				}
			} else {
				ret = playMove(nextPlayerIndex, terminalDepth-nextDepth, methodDepth+1, nextPlayerIndex == playingAs, alpha, beta);
			}
			
			unplayMove(m);
			
			if(maxPlayer) {
				alpha = Math.max(alpha, ret);
				aj = alpha;
			} else {
				beta = Math.min(beta, ret);
				aj = beta;
			}
			//if(terminalDepth == startDepth - 8) {
			//}
			if(beta <= alpha) { break; }
		}
		return aj;
	}

	private List<Move> getMoves(int player) {
		List<Move> moves = new ArrayList<Move>();
		if(q.peekLast() == null || roundWinner == player) {
			for(int i = 0; i < numbersOnCard; i++) {
				for(int j = 1; j <= hands[player][i]; j++) {
					// If statement makes the person not split cards
					if(j == hands[player][i]) {
						Move m = new Move(player, i, j, MoveType.CARD);
						moves.add(m);
					}
				}
			}
		} else {
			for(int i = 0; i < numbersOnCard; i++) {
				if(i > roundWinCardNo && hands[player][i] >= roundWinAmount) {
					Move m = new Move(player, i, roundWinAmount, MoveType.CARD);
					moves.add(m);
				}
			}
			moves.add(new Move(player, 0, 0, MoveType.PASS));
		}
		return moves;
	}

	private void printMove(Move m, int i, int alpha, int beta) {
		print("P: " + m.playerId + " A:" + alpha + " B:" + beta + " M: {" + m + "}", i);
	}

	private void unplayMove(Move m) {
		hands[m.playerId][m.cardNumber] += m.amount;
		q.removeLast();
		if(m.type != MoveType.PASS) {
			roundWinner = m.playerId;
			roundWinAmount = m.amount;
			roundWinCardNo = m.cardNumber;
		}
	}

	private void playMove(Move m) {
		if(m.type != MoveType.PASS) {
			roundWinner = m.playerId;
			roundWinAmount = m.amount;
			roundWinCardNo = m.cardNumber;
		}
		q.addLast(m);
		hands[m.playerId][m.cardNumber] -= m.amount;
	}

	private void initCardsStatic() {
		hands = new int[players][numbersOnCard];
		for(int i = 0; i < players; i++) {
			for(int j = 0; j < numbersOnCard; j++) {
				hands[i][j] = 0;
			}
		}
		
		// 4 Players
		hands[0][0]  = 1; hands[0][1]  = 1; hands[0][2]  = 3; hands[0][3]  = 4; hands[0][4]   = 2; hands[0][5]   = 1;
		hands[0][6]  = 3; hands[0][7]  = 4; hands[0][8]  = 5; hands[0][9]  = 4; hands[0][10]  = 5; hands[0][11]  = 3;
		hands[1][0]  = 1; hands[1][1]  = 5; hands[1][2]  = 2; hands[1][3]  = 4; hands[1][4]   = 5; hands[1][5]   = 3;
		hands[1][6]  = 3; hands[1][7]  = 3; hands[1][8]  = 2; hands[1][9]  = 2; hands[1][10]  = 2; hands[1][11]  = 4;
		hands[2][0]  = 8; hands[2][1]  = 3; hands[2][2]  = 4; hands[2][3]  = 1; hands[2][4]   = 1; hands[2][5]   = 3;
		hands[2][6]  = 4; hands[2][7]  = 1; hands[2][8]  = 1; hands[2][9]  = 3; hands[2][10]  = 2; hands[2][11]  = 5;
		hands[3][0]  = 2; hands[3][1]  = 3; hands[3][2]  = 3; hands[3][3]  = 3; hands[3][4]   = 4; hands[3][5]   = 5;
		hands[3][6]  = 2; hands[3][7]  = 4; hands[3][8]  = 4; hands[3][9]  = 3; hands[3][10]  = 3; hands[3][11]  = 0;
		
		// 8 Players
		//hands[0][0]  = 0; hands[0][1]  = 0; hands[0][2]  = 0; hands[0][3]  = 1; hands[0][4]   = 3; hands[0][5]   = 0;
		//hands[0][6]  = 3; hands[0][7]  = 1; hands[0][8]  = 3; hands[0][9]  = 0; hands[0][10]  = 3; hands[0][11]  = 4;
		//hands[1][0]  = 3; hands[1][1]  = 2; hands[1][2]  = 3; hands[1][3]  = 0; hands[1][4]   = 1; hands[1][5]   = 1;
		//hands[1][6]  = 1; hands[1][7]  = 3; hands[1][8]  = 1; hands[1][9]  = 0; hands[1][10]  = 1; hands[1][11]  = 2;
		//hands[2][0]  = 3; hands[2][1]  = 1; hands[2][2]  = 1; hands[2][3]  = 1; hands[2][4]   = 1; hands[2][5]   = 1;
		//hands[2][6]  = 3; hands[2][7]  = 1; hands[2][8]  = 2; hands[2][9]  = 1; hands[2][10]  = 2; hands[2][11]  = 1;
		//hands[3][0]  = 1; hands[3][1]  = 0; hands[3][2]  = 3; hands[3][3]  = 3; hands[3][4]   = 2; hands[3][5]   = 2;
		//hands[3][6]  = 0; hands[3][7]  = 3; hands[3][8]  = 1; hands[3][9]  = 0; hands[3][10]  = 3; hands[3][11]  = 0;
		//hands[4][0]  = 2; hands[4][1]  = 2; hands[4][2]  = 0; hands[4][3]  = 2; hands[4][4]   = 1; hands[4][5]   = 3;
		//hands[4][6]  = 0; hands[4][7]  = 1; hands[4][8]  = 1; hands[4][9]  = 2; hands[4][10]  = 1; hands[4][11]  = 3;
		//hands[5][0]  = 0; hands[5][1]  = 2; hands[5][2]  = 0; hands[5][3]  = 2; hands[5][4]   = 1; hands[5][5]   = 2;
		//hands[5][6]  = 3; hands[5][7]  = 1; hands[5][8]  = 2; hands[5][9]  = 4; hands[5][10]  = 1; hands[5][11]  = 0;
		//hands[6][0]  = 1; hands[6][1]  = 2; hands[6][2]  = 2; hands[6][3]  = 1; hands[6][4]   = 2; hands[6][5]   = 2;
		//hands[6][6]  = 0; hands[6][7]  = 1; hands[6][8]  = 1; hands[6][9]  = 3; hands[6][10]  = 1; hands[6][11]  = 2;
		//hands[7][0]  = 2; hands[7][1]  = 3; hands[7][2]  = 3; hands[7][3]  = 2; hands[7][4]   = 1; hands[7][5]   = 1;
		//hands[7][6]  = 2; hands[7][7]  = 1; hands[7][8]  = 1; hands[7][9]  = 2; hands[7][10]  = 0; hands[7][11]  = 0;
	}
	
	private void initCards() {
		
		hands = new int[players][numbersOnCard];
		int cardCount[] = new int[amountOfEachCard];
		for(int i = 0; i < numbersOnCard; i++) {
			cardCount[i] = amountOfEachCard;
		}
		for(int i = 0; i < players; i++) {
			for(int j = 0; j < numbersOnCard; j++) {
				hands[i][j] = 0;
			}
		}
		thisGameCardCount = totalCards;
		while(thisGameCardCount > 0) {
			for(int i = players - 1; i >= 0 && thisGameCardCount > 0; i--) {
				int ret = (int)(Math.random() * numbersOnCard);
				if(cardCount[ret] == 0) {
					for(int j = ret; j < (numbersOnCard * 2); j++) {
						ret = (j % numbersOnCard);
						if(cardCount[ret] != 0) break;
					}
				}
				cardCount[ret]--;
				hands[i][ret]++;
				thisGameCardCount--;
			}
		}
	}
	
	/*private int playersLeft() {
		int count = 0;
		int playCount = 0;
		for(int k = 0; k < players; k++) {
			count = 0;
			for(int i = 0; i < numbersOnCard; i++) {
				count += hands[k][i];
			}
			if(count > 0) playCount++;
		}
		return playCount;
	}*/

	/*private int gameOver() {
		int count = 0;
		for(int k = 0; k < players; k++) {
			for(int i = 0; i < numbersOnCard; i++) {
				count += hands[k][i];
			}
		}
		return count;
	}*/
	
	private void payTaxes() {
		// The Number of players having to switch cards
		int noptpt = 2;
		for(int j = noptpt; j > 0; j--) {
			int popperplayer = noptpt - j + 1;
			int kingplayer = noptpt - j;
			// The Number of times to switch cards
			for(int k = j; k > 0; k--) {
				// Pay to the rich bestcard
				int bestcard = numbersOnCard - 1;
				for(int i = bestcard; i > 0; i--) {
					if(hands[players - popperplayer][i] != 0) {
					hands[players - popperplayer][i]--;
					hands[kingplayer][i]++;
					break;
					}
				}
				// Pay to the poor worst card
				int worstcard = 0;
				for(int i = worstcard; i < numbersOnCard; i++) {
					if(hands[kingplayer][i] != 0) {
						hands[kingplayer][i]--;
						hands[players - popperplayer][i]++;
						break;
					}
				}
			}
		}
	}
	
	private boolean playerHasWon(int playerid) {
		int count = 0;
		for(int i = 0; i < numbersOnCard; i++) {
			count += hands[playerid][i];
		}
		return count == 0;
	}
	
	private void showMoves() {
		initCardsStatic();
		for(int i = 0; i < q.size(); i++) {
			Move m = q.get(i);
			playMove(m);
			showCards(i);
			print("P: " + m.playerId + " M: {" + m + "} ", i);
		}
		out.flush();
		System.exit(0);
	}
	
	private void showCards(int depth) {
		for(int k = 0; k < players; k++) {
			for(int i = 0; i < depth; i++) { out.print(" "); }
			out.print("Player: " + k);
			int value = 0;
			for(int i = 0; i < hands[k].length; i++) {
				out.print(" Card(" + i + ")=" + hands[k][i] + " ");
				value += ((amountOfEachCard - i) * (numbersOnCard - hands[k][i]) * 100);
			}
			out.print("Value: " + value + " ");
			out.println();
		}
		for(int i = 0; i < depth; i++) { out.print(" "); }
		out.println();
	}
	
	private void print(String in, int depth) {
		//if(depth == 310) System.out.println(depth);
		for(int i = 0; i < depth; i++) {
			out.print(" ");
		}
		out.println(in + " D: " + depth);
		out.flush();
	}
}
