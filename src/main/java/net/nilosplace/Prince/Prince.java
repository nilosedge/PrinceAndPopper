package net.nilosplace.Prince;

import java.util.*;

import net.nilosplace.Prince.Players.*;

public class Prince {
	Dealer deal;
	
	public Prince() {

		List<Player> players = new ArrayList<Player>();
		
		players.add(new RandomPlayer(0));
		players.add(new RandomPlayer(1));
		players.add(new ThriftyPlayer(2));
		players.add(new ThriftyPlayer(3));
		players.add(new NonSplitPlayer(4));
		players.add(new NonSplitPlayer(5));
		players.add(new SpendyPlayer(6));
		players.add(new AlwaysSplitPlayer(7));

		//players.add(new AlwaysSplitPlayer(0));
		//players.add(new AlwaysSplitPlayer(1));
		//players.add(new NonSplitPlayer(2));
		//players.add(new NonSplitPlayer(3));
		//players.add(new RandomPlayer(4));
		//players.add(new RandomPlayer(5));
		//players.add(new SpendyPlayer(6));
		//players.add(new SpendyPlayer(7));

		deal = new Dealer(players);
		GamePlay gp = new GamePlay(deal, players);
		gp.start();
	}
}
