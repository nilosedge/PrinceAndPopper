package net.nilosplace.cards;

import java.util.*;
import java.util.stream.*;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
public class GameState {

	private Double[] usedCards;
	private Double[] tableCards;
	private Double[] handCards;
	private Double[] availableMoves;
	private Double[] ourPosition;
	private Double[] winPosition;
	private Double[] cardCounts;
	private Double[] canPass = new Double[1];
	private Double[] simpleGameState = new Double[77];
	private Double[][] fullGameState = new Double[77][];

	ObjectMapper mapper = new ObjectMapper();

	public GameState(Deck deck, Table table, int startingPlayerCount, List<CardPlayer> players, int playersTurn, int roundWinner, int maxHandSize) {

		Collection<List<Card>> list = deck.getDeck().stream().collect(Collectors.groupingBy(Card::getCardValue)).values();

		usedCards = new Double[deck.getHighestCard() + 1];
		for(List<Card> cards: list) {
			usedCards[cards.get(0).getCardValue()] = (double)cards.size() / deck.getHighestCard();
		}

		tableCards = new Double[deck.getHighestCard() + 1];
		for(List<Card> cards: table.getStack()) {
			tableCards[cards.get(0).getCardValue()] = (double)cards.size() / deck.getHighestCard();
		}

		list = players.get(playersTurn).getHand().getHand().stream().collect(Collectors.groupingBy(Card::getCardValue)).values();

		handCards = new Double[deck.getHighestCard() + 1];
		for(List<Card> cards: list) {
			handCards[cards.get(0).getCardValue()] = (double)cards.size() / deck.getHighestCard();
		}

		// availableMoves
		List<Move> moves;
		if(table.getStack().size() == 0) {
			moves = players.get(playersTurn).getHand().getFirstMoves();
			canPass[0] = 0.0;
		} else {
			moves = players.get(playersTurn).getHand().getMoves(table.getHighest(), table.getAmount());
			canPass[0] = 1.0;
		}

		availableMoves = new Double[deck.getHighestCard() + 1];
		for(Move move: moves) {
			availableMoves[move.getCards().get(0).getCardValue()] = (double)move.getCards().size() / deck.getAmountOfEachCard();
		}

		ourPosition = new Double[startingPlayerCount];
		winPosition = new Double[startingPlayerCount];
		ourPosition[playersTurn] = 1.0;
		if(roundWinner == -1) {
			winPosition[playersTurn] = 1.0;
		} else {
			winPosition[roundWinner] = 1.0;
		}


		List<Integer> counts = players.stream().map(CardPlayer::getCardCount).collect(Collectors.toList());
		cardCounts = new Double[startingPlayerCount];
		for(int i = 0; i < counts.size(); i++) {
			cardCounts[i] = (double)counts.get(i) / (double)maxHandSize;
		}

		
		Random generator = new Random(11223344);

		List<Double[]> arrayList = new ArrayList<>();
		arrayList.add(usedCards);
		arrayList.add(tableCards);
		arrayList.add(handCards);
		arrayList.add(availableMoves);
		arrayList.add(ourPosition);
		arrayList.add(winPosition);
		arrayList.add(cardCounts);
		arrayList.add(canPass);

		List<Double> result = new ArrayList<>();
		for (Double[] array: arrayList) {
	        Collections.addAll(result, array);
	    }
		simpleGameState = result.toArray(new Double[0]);
		
		for(int i = 0; i < 77; i++) {
			Collections.shuffle(arrayList, generator);
			result.clear();
			for (Double[] array: arrayList) {
		        Collections.addAll(result, array);
		    }
			fullGameState[i] = result.toArray(new Double[0]);
		}
	}

}
