package net.nilosplace.cards;

import java.util.List;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import lombok.Data;

@Data
public class CardPlayer {
	
	private Hand hand = new Hand();
	private int score = 0;
	
	private SummaryStatistics stats = new SummaryStatistics();

	public Move getFirstMove() {
		List<Move> moves = hand.getFirstMoves();
		
		hand.removeCards(moves.get(0));
		return moves.get(0);
	}

	public Move getMove(int highest, int amount) {
		List<Move> moves = hand.getMoves(highest, amount);
		if(moves.size() > 0) {
			stats.addValue(moves.size() + 1);
			hand.removeCards(moves.get(0));
			return moves.get(0);
		} else {
			stats.addValue(1l);
			return null;
		}
	}

	public int getCardCount() {
		return hand.getCardCount();
	}

	public void addCards(List<Card> cards) {
		hand.addCards(cards);
	}

	public void addPoints(int points) {
		score += points;
	}


}
