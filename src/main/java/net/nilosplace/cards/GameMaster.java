package net.nilosplace.cards;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.commons.math3.stat.descriptive.SummaryStatistics;

import lombok.extern.log4j.Log4j2;

@Log4j2
public class GameMaster {

	public static void main(String[] args) {
		new GameMaster();
	}

	public GameMaster() {

		Table table = new Table();
		Deck deck = new Deck();

		int playerCount = 8;

		// Give out hands
		List<CardPlayer> wonPlayers = new ArrayList<CardPlayer>();
		List<CardPlayer> players = new ArrayList<CardPlayer>();
		for(int i = playerCount - 1; i >= 0; i--) {
			players.add(new CardPlayer());
		}





		
		SummaryStatistics stats = new SummaryStatistics();

		for(int inc = 0; inc < 1000; inc++) {

			int numberOfMoves = 0;

			if(players.isEmpty()) {
				players.addAll(wonPlayers);
				wonPlayers.clear();
			}


			//log.info(players.stream().map(CardPlayer::getScore).collect(Collectors.toCollection(ArrayList::new)));
			LinkedList<Hand> hands = players.stream().map(CardPlayer::getHand).collect(Collectors.toCollection(LinkedList::new));

			// Deal cards into hands
			deck.shuffle();
			int c = 0;
			int maxHandSize = 0;
			while(deck.hasCards()) {
				hands.get(c).addCard(deck.dealCard());
				if(hands.get(c).getCardCount() > maxHandSize) maxHandSize = hands.get(c).getCardCount();
				c++;
				c %= playerCount;
			}

			//Assign taxes
			hands.get(hands.size() - 1).addCard(hands.get(0).getHighestCard());
			hands.get(hands.size() - 1).addCard(hands.get(0).getHighestCard());
			hands.get(0).addCard(hands.get(hands.size() - 1).getLowestCard());
			hands.get(0).addCard(hands.get(hands.size() - 1).getLowestCard());
			hands.get(hands.size() - 2).addCard(hands.get(1).getHighestCard());
			hands.get(1).addCard(hands.get(hands.size() - 2).getLowestCard());

			int playersTurn = 0;
			int roundWinner = 0;
			int playersLeft = playerCount;

			while(playersLeft > 0) {

				
				
				Move move = null;
				if(playersTurn == roundWinner) {
					deck.addCards(table.clearCards());
					
					GameState state = new GameState(deck, table, playerCount, players, playersTurn, roundWinner, maxHandSize);
					log.info(state.getSimpleGameState());
					
					move = players.get(playersTurn).getFirstMove();
					table.stackCards(move.getCards());
				} else {
					
					GameState state = new GameState(deck, table, playerCount, players, playersTurn, roundWinner, maxHandSize);
					log.info(state.getSimpleGameState());
					
					move = players.get(playersTurn).getMove(table.getHighest(), table.getAmount());
					if(move != null) {
						if(move.isValidMove(table.getHighest(), table.getAmount())) {
							table.stackCards(move.getCards());
							//					} else {
							//						// Wrong Count player passes
							//						players.get(playersTurn).addCards(move.getCards());
							//						log.info("Player: " + playersTurn + " passes (invalid move)");
						}
					} else {
						// Player passes
						//log.info("Player: " + playersTurn + " passes");
					}
					if(roundWinner == -1) roundWinner = playersTurn;
				}

				if(move != null) {
					log.info("Player: " + playersTurn + " plays: " + move + " card count: " + players.get(playersTurn).getCardCount());
					roundWinner = playersTurn;
				}

				if(players.get(playersTurn).getCardCount() == 0) {
					CardPlayer player = players.remove(playersTurn);
					player.addPoints(playerCount - wonPlayers.size());
					wonPlayers.add(player);
					playersLeft--;
					roundWinner = -1;
					if(playersLeft == 0) {
						deck.addCards(table.clearCards());
						break;
					}
				} else {
					if(move == null || move.getCards().get(0).getCardValue() != deck.getHighestCard()) {
						playersTurn++;
					}
				}
				numberOfMoves++;

				playersTurn %= playersLeft;
				roundWinner %= playersLeft; // In case the last person wins it needs to role to person 0
			}
			stats.addValue(numberOfMoves);
			
			
			log.info("Game Ended: " + wonPlayers.stream().map(CardPlayer::getScore).collect(Collectors.toCollection(ArrayList::new)));
		}

		//log.info(stats);
		//log.info(wonPlayers.stream().map(CardPlayer::getStats).collect(Collectors.toCollection(ArrayList::new)));
		//log.info(deck.getDeck().size());
	}

}
