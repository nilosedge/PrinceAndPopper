package net.nilosplace.cards;

import java.util.*;

import lombok.Data;

@Data
public class Deck {

	private List<Card> deck;
	private int highestCard = 0;
	private int amountOfEachCard = 0;

	public Deck() {
		this(13, 12, 1);
	}

	public Deck(int numbersOnCard, int amountOfEachCard, int numbersOfDecks) {
		deck = new ArrayList<Card>();
		highestCard = numbersOnCard - 1;
		this.amountOfEachCard = amountOfEachCard;
		
		for(int j = 0; j < numbersOfDecks; j++) {
			for(int i = 0; i < numbersOnCard; i++) {
				for(int k = 0; k < amountOfEachCard; k++) {
					deck.add(new Card(i));
				}
			}
		}
	}
	
	public boolean hasCards() {
		return deck.size() > 0;
	}

	public void shuffle() {
		Collections.shuffle(deck);
	}

	public Card dealCard() {
		return deck.remove(0);
	}

	public void addCards(List<Card> cards) {
		deck.addAll(cards);
	}

}
