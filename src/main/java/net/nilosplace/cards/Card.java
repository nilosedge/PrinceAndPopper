package net.nilosplace.cards;

import lombok.Data;

@Data
public class Card {

	private int cardValue;
	
	public Card(int cardValue) {
		this.cardValue = cardValue;
	}

}
