package net.nilosplace.cards;

import java.util.List;

import lombok.Data;

@Data
public class Move {
	
	private List<Card> cards;
	
	public Move(List<Card> cards) {
		this.cards = cards;
	}
	
	public boolean isValidMove(int currentHighest, int currentAmount) {
		if(cards.size() != currentAmount) return false;
		int cardValue = cards.get(0).getCardValue();
		for(Card c: cards) {
			if(c.getCardValue() != cardValue) {
				return false;
			}
		}
		return true;
	}


}
