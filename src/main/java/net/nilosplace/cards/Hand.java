package net.nilosplace.cards;

import java.util.*;
import java.util.stream.Collectors;

import lombok.Data;
import lombok.extern.log4j.Log4j2;

@Data
@Log4j2
public class Hand {

	private LinkedList<Card> hand;
	
	public Hand() {
		hand = new LinkedList<Card>();
	}
	
	public void addCard(Card c) {
		hand.add(c);
		sortHand();
	}
	
	public void addCards(List<Card> cards) {
		hand.addAll(cards);
		sortHand();
	}
	
	public int getCardCount() {
		return hand.size();
	}
	
	public void sortHand() {
		//log.info("Before: " + hand);
		hand.sort(new Comparator<Card>() {
			public int compare(Card o1, Card o2) {
				//log.info(o1 + " " + o2 + " = " + (o1.getCardValue() - o2.getCardValue()));
				return o2.getCardValue() - o1.getCardValue();
			}
		});
		//log.info("After: " + hand);
	}
	
	public Card getHighestCard() {
		Card ret = hand.removeFirst();
		sortHand();
		return ret;
	}
	
	public Card getLowestCard() {
		Card ret = hand.removeLast();
		sortHand();
		return ret;
	}

	
	public List<Move> getFirstMoves() {
		Collection<List<Card>> computedMoves = hand.stream().collect(Collectors.groupingBy(Card::getCardValue)).values();
		List<Move> ret = new ArrayList<Move>();
		for(List<Card> list: computedMoves) {
			ret.add(new Move(list));
		}
		return ret;
	}
	
	public List<Move> getMoves(int highest, int amount) {
		Collection<List<Card>> computedMoves = hand.stream().collect(Collectors.groupingBy(Card::getCardValue)).values();
		List<Move> ret = new ArrayList<Move>();
		for(List<Card> list: computedMoves) {
			if(list.get(0).getCardValue() > highest && list.size() >= amount) {
				ret.add(new Move(list.subList(0, amount)));
			}
		}
		return ret;
	}

	public void removeCards(Move move) {
		for(Card c: move.getCards()) {
			hand.remove(c);
		}
	}
}
