package net.nilosplace.cards;

import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.learning.config.Adam;
import org.nd4j.linalg.lossfunctions.LossFunctions;

public class NetworkBrain {


	public static void main(String[] args) throws Exception {

		//Random rand = new Random();

		int numberOfChannels = 128;
		int possiableMoves = 13;
		
		MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
				.seed(11223345)
				.l2(0.0005)
				.weightInit(WeightInit.XAVIER)
				.updater(new Adam(1e-3))
				.list()
				.layer(new Convolution2D.Builder(11, 11).nIn(numberOfChannels).stride(1,1).nOut(300).activation(Activation.RELU).build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(1,1).build())
				.layer(new Convolution2D.Builder(4, 4).stride(1,1).nOut(75).activation(Activation.RELU).build())
				.layer(new SubsamplingLayer.Builder(PoolingType.MAX).kernelSize(2,2).stride(1,1).build())
				
				.layer(new DenseLayer.Builder().activation(Activation.RELU).nOut(500).build())
				
				.layer(new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD).nOut(possiableMoves).activation(Activation.SOFTMAX).build())
				.setInputType(InputType.convolutionalFlat(77,77,1)) //See note below
				.build();
	}


}
