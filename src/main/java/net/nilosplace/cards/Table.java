package net.nilosplace.cards;

import java.util.*;

import lombok.Data;

@Data
public class Table {

	LinkedList<List<Card>> stack = new LinkedList<List<Card>>();

	public void stackCards(List<Card> playerMove) {
		stack.add(playerMove);
	}

	public List<Card> clearCards() {
		List<Card> ret = new ArrayList<Card>();
		for(List<Card> cards: stack) {
			ret.addAll(cards);
		}
		stack.clear();
		return ret;
	}

	public int getAmount() {
		return stack.getLast().size();
	}

	public int getHighest() {
		return stack.getLast().get(0).getCardValue();
	}
	
	
}
